package hindidictionary.bdrulez.com.retrofitexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import hindidictionary.bdrulez.com.retrofitexample.adapter.SedeAdapter;

public class MainActivity extends AppCompatActivity {

    private RecyclerView rviSedes;
    private SedeAdapter adapter;

    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rviSedes = findViewById(R.id.rviSedes);
    }

    public void configurarClinicas() {

        adapter = new SedeAdapter(this);
        layoutManager = new LinearLayoutManager(this);

        rviSedes.setHasFixedSize(true);
        rviSedes.setLayoutManager(layoutManager);
        rviSedes.setAdapter(adapter);


    }
}
