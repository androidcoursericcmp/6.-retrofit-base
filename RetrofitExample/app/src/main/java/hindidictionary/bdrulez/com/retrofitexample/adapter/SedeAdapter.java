package hindidictionary.bdrulez.com.retrofitexample.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import hindidictionary.bdrulez.com.retrofitexample.R;


/**
 * Created by riccMP on 14/09/2018.
 */

public class SedeAdapter extends RecyclerView.Adapter<SedeAdapter.ViewHolder> {


    private Context mContext;

    public SedeAdapter(Context context) {
        this.mContext = context;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_clinica, parent, false);

        return new ViewHolder(v, mContext);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int position) {


    }


    @Override
    public int getItemCount() {
        return 4;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tviNombreSede;
        ImageView iviClinica;
        Context context;

        public ViewHolder(View view, Context context) {
            super(view);
            this.context = context;
            tviNombreSede = view.findViewById(R.id.tviNombreSede);
            iviClinica = view.findViewById(R.id.iviClinica);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Toast.makeText(context, "Click en la clinica " + tviNombreSede.getText().toString(), Toast.LENGTH_SHORT).show();
        }
    }


}